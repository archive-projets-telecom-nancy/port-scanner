#!/bin/sh

find . -type d | sed 's/^.\///' | sed '/^out/d' | sed '/\./d' | sed '/^\s*$/d'