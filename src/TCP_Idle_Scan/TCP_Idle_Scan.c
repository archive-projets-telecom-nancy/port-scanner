#include <arpa/inet.h>
#include <fcntl.h>
#include <ifaddrs.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

#include "TCP_Idle_Scan.h"
#include "top_1000_tcp_ports.h"

#include "../Utilitaires/vecteur.h"
#include "../Utilitaires/myIP.h"
#include "../Utilitaires/myTCP.h"
#include "../Utilitaires/ip_checksum.h"

void TCP_Idle_Scan(vecteur* adresses, uint32_t zombie_addr, uint16_t zombie_port) {

	int sock_fd = creer_socket_idle();

	printf("TCP Idle Scan\n");

	printf("Choix d'une machine zombie\n");
	zombie zombie;
	zombie.adresse_hote = recuperer_adresse_host();
	zombie.adresse_zombie = zombie_addr;
	zombie.port = zombie_port;
	/*
	if (trouver_zombie(adresses, sock_fd, &zombie) < 0) {
		printf("Impossible de trouver une machine zombie !\nAnnulation du scan\n");
		return;
	} else {
		char buff[16];
		printf("Zombie trouvé : %s\n",inet_ntop(AF_INET,(struct in_addr*) &zombie,buff,15));
	}
	*/
	char buff[16];
	for (int i = 0; i < adresses->taille; i++) {
		uint32_t* adresse = (uint32_t*) adresses->contenu[i];
		printf("Scan de %s\n",inet_ntop(AF_INET,(struct in_addr*) adresse,buff,15));
		Idle_Scan_with_zombie(&zombie, *adresse, sock_fd);
	}

	close(sock_fd);

}

void Idle_Scan_with_zombie(zombie* zombie,uint32_t cible, int sock_fd) {

	for (int i = 0; i < nombre_de_ports; i++) {

		uint16_t avant;
		uint16_t apres;

		if(recuperer_ip_id(&avant, zombie, sock_fd) < 0) {
			char buff[16];
			printf("Impossible de récuperer l'IP ID de %s avant de spoofer",inet_ntop(AF_INET,(struct in_addr*) &zombie,buff,15));
			return;
		}

		envoyer_SYN_spoof(sock_fd, zombie, cible, ports[i]);

		if(recuperer_ip_id(&apres, zombie, sock_fd) < 0) {
			char buff[16];
			printf("Impossible de récuperer l'IP ID de %s après avoir spoofé",inet_ntop(AF_INET,(struct in_addr*) &zombie,buff,15));
			return;
		}

		uint16_t diff = avant - apres;

		if (diff == 2) {
			printf("Port ouvert : %d\n",ports[i]);
		}
		printf("\r%d/%d",i,nombre_de_ports);
	}
}

int creer_socket_idle() {

	int socket_raw;
	if((socket_raw=socket(AF_INET, SOCK_RAW, IPPROTO_TCP)) < 0) {
		perror("Erreur lors de la création de la socket ");
		exit(-1);
	}
	if(fcntl(socket_raw, F_SETFL, O_NONBLOCK) < 0) {
		perror("Erreur lors de l'ajout de l'option non bloquante ");
		exit(-1);
	}
	int on = 1;
	if(setsockopt(socket_raw, IPPROTO_IP, IP_HDRINCL, &on, sizeof(on)) < 0) {
		perror("Erreur lors de l'ajout de l'option Header Included");
	}
	return socket_raw;

}

int trouver_zombie(vecteur* adresses, int sock_fd, zombie* zombie) {
	return -1;
}

int recuperer_ip_id(uint16_t* ip_id, zombie* zombie, int sock_fd) {

	if (envoyer_SYN_ACK(sock_fd, zombie) < 0) {
		printf("Erreur lors de l'envoi d'un SYN/ACK");
		return -1;
	}
	time_t envoi = clock();
	while (((clock()-envoi) / CLOCKS_PER_SEC) <= 2.0 ) {
		if(recevoir_ip_id(ip_id, zombie, sock_fd) >= 0) {
			return 0;
		}
		// on attend ~0.05 secondes
		usleep(50000);
	}
	return -1;
} 

int recevoir_ip_id(uint16_t* ip_id, zombie* zombie, int sock_fd) {

	size_t taille_buffer = sizeof(struct iphdr)+sizeof(struct my_tcp_header);
	void* buffer = (void*) calloc(1,taille_buffer);

	struct sockaddr_storage adresse_cible;
	socklen_t taille_adresse_cible = sizeof(struct sockaddr_storage);

	while (
		recvfrom(
			sock_fd,
			buffer,
			taille_buffer,
			0,
			(struct sockaddr*) &adresse_cible,
			&taille_adresse_cible
		)
		> 0
	) {
		struct iphdr* ip = (struct iphdr*) buffer;
		struct my_tcp_header* tcp = (struct my_tcp_header*) (buffer+sizeof(struct iphdr));
		// si c'est l'adresse et le port source sont bons
		if (ip->saddr == zombie->adresse_zombie && tcp->source == zombie->port) {
			// si c'est un RST
			if (tcp->flags == 4) {
				*ip_id = ip->id;
				free(buffer);
				return 0;
			}
		}
	}

	free(buffer);
	return -1;

}

int envoyer_SYN_spoof(int sock_fd, zombie* zombie, uint32_t cible, uint16_t port) {

	size_t taille_buffer = sizeof(struct iphdr)+sizeof(struct my_tcp_header);
	void* buffer = (void*) calloc(1,taille_buffer);

	struct iphdr* ip = (struct iphdr*) buffer;
	struct my_tcp_header* tcp = (struct my_tcp_header*) (buffer+sizeof(struct iphdr));

	ip->version = 4;
	ip->ihl = 5;
	ip->tos = 0;
	ip->tot_len = 0;
	ip->id = 0;
	ip->frag_off = 0;
	ip->ttl = 255;
	ip->protocol = IPPROTO_TCP;
	ip->saddr = zombie->adresse_zombie;
	ip->daddr = cible;

	tcp->source = htons(zombie->port);
	tcp->dest = htons(port);
	tcp->seq = htonl(rand());
	tcp->ack_seq = htonl(rand());
	tcp->doff = 0b01010000;
	//             CEUAPRSF
	//             WCRCSSYI
	//             REGKHTNN
	tcp->flags = 0b00000010;
	tcp->window = 0;
	tcp->check = 0;
	tcp->urg_ptr = 0;
	tcp->check = tcp_checksum(ip->saddr, ip->daddr, tcp);

	// réglage de l'adresse d'envoi
	struct in_addr* dest_addr = (struct in_addr*) &(zombie->adresse_zombie);

	struct sockaddr_in servaddr;
	memset(&servaddr, 0, sizeof(struct sockaddr_in));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = dest_addr->s_addr;

	return sendto (
		sock_fd,
		buffer,
		taille_buffer,
		0,
		(struct sockaddr *)&servaddr,
		sizeof(struct sockaddr_in)
	);

}

int envoyer_SYN_ACK(int sock_fd, zombie* zombie) {
	
	size_t taille_buffer = sizeof(struct iphdr)+sizeof(struct my_tcp_header);
	void* buffer = (void*) calloc(1,taille_buffer);

	struct iphdr* ip = (struct iphdr*) buffer;
	struct my_tcp_header* tcp = (struct my_tcp_header*) (buffer+sizeof(struct iphdr));

	ip->version = 4;
	ip->ihl = 5;
	ip->tos = 0;
	ip->tot_len = 0;
	ip->id = 0;
	ip->frag_off = 0;
	ip->ttl = 255;
	ip->protocol = IPPROTO_TCP;
	ip->saddr = 0;
	ip->daddr = zombie->adresse_zombie;

	tcp->source = htons(rand());
	tcp->dest = htons(zombie->port);
	tcp->seq = htonl(rand());
	tcp->ack_seq = htonl(rand());
	tcp->doff = 0b01010000;
	//             CEUAPRSF
	//             WCRCSSYI
	//             REGKHTNN
	tcp->flags = 0b00010010;
	tcp->window = 0;
	tcp->check = 0;
	tcp->urg_ptr = 0;
	tcp->check = tcp_checksum(zombie->adresse_hote, zombie->adresse_zombie, tcp);

	// réglage de l'adresse d'envoi
	struct in_addr* dest_addr = (struct in_addr*) &(zombie->adresse_zombie);

	struct sockaddr_in servaddr;
	memset(&servaddr, 0, sizeof(struct sockaddr_in));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = dest_addr->s_addr;

	return sendto (
		sock_fd,
		buffer,
		taille_buffer,
		0,
		(struct sockaddr *)&servaddr,
		sizeof(struct sockaddr_in)
	);

}

uint32_t recuperer_adresse_host() {

    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;

    getifaddrs(&ifAddrStruct);
    uint32_t addr = 0;

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) {
        	uint32_t tmpaddr = ((struct sockaddr_in *)ifa->ifa_addr)->sin_addr.s_addr;
        	if (tmpaddr != 16777343) {
        		addr = tmpaddr;
        	}
        }
    }
    if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);

    if (addr == 0) {
    	printf("Impossible de récuperer l'adresse hôte !\n");
    	exit(-1);
    }
    
    return addr;

}