#ifndef TCP_IDLE_SCAN_H
#define TCP_IDLE_SCAN_H

#include <stdint.h>

#include "../Utilitaires/vecteur.h"

typedef struct zombie zombie;

struct zombie {
	uint32_t adresse_hote;
	uint32_t adresse_zombie;
	uint16_t port;
};

void TCP_Idle_Scan(vecteur* adresses, uint32_t zombie_addr, uint16_t zombie_port);
void Idle_Scan_with_zombie(zombie* zombie,uint32_t cible, int sock_fd);
//int trouver_zombie(vecteur* adresses, int sock_fd, zombie* zombie);
int creer_socket_idle();
int recuperer_ip_id(uint16_t* ip_id, zombie* zombie, int sock_fd);
int recevoir_ip_id(uint16_t* ip_id, zombie* zombie, int sock_fd);
int envoyer_SYN_spoof(int sock_fd, zombie* zombie, uint32_t cible, uint16_t port);
int envoyer_SYN_ACK(int sock_fd, zombie* zombie);
uint32_t recuperer_adresse_host();

#endif