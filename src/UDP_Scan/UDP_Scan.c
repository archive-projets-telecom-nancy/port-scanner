#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/timeb.h>

#include "UDP_Scan.h"

int udp_scan(char *dest, int port) {

    //int portTest;
    int sckt;
    //int res;
    int n;
    //fd_set set;
    struct sockaddr_in sin;
    //struct timeval timeout;
    struct timeb tp;
    char *data;
    char sendbuf[1500];

    /* 
    * Remplir la structure  serv_addr avec l'adresse du serveur 
    */
    memset( (char *) &sin,0, sizeof(sin) );
    sin.sin_family = PF_INET;
    sin.sin_addr.s_addr = inet_addr(dest);
    sin.sin_port = htons(port);

    inet_pton(AF_INET, dest, &sin.sin_addr.s_addr);
    printf("Try to open UDP socket. \n");
    if ((sckt = socket(PF_INET, SOCK_DGRAM, 0)) <0) {
        printf("Fail to open UDP socket\n");
        exit(1);
    }
    printf("UDP socket opened !\n");
    int ttl=1;
    if (setsockopt(sckt, IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl)) <0){
        perror ("setsockopt");
        exit (1);
    }

    ftime(&tp);
    data =ctime(&tp.time);

    if ((n = sendto(sckt, data, strlen(data),0,(struct sockaddr *)&sin, sizeof(struct sockaddr_in))) != strlen(data)){
        printf("Sendto error\n");
    }
    socklen_t len = sizeof(sin);
    if ( (n= recvfrom (sckt, sendbuf, sizeof(sendbuf)-1,0, (struct sockaddr *)&sin, &len)) != strlen(data) )  {
        printf ("erreur recvfrom");
        exit (1);
    }

    //TODO : vérifier l'état du paquet reçu
    // si ICMP "port unreachable", port fermé
    // sinon ok

    close(sckt);
    printf("Fail to connect\n");
    exit(1);
}
