#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "ip_checksum.h"
#include "hex_dump.h"
#include "../Utilitaires/myTCP.h"

uint16_t ip_checksum(void* vdata,size_t length) {
	
	// Cast the data pointer to one that can be indexed.
	char* data=(char*)vdata;

	// Initialise the accumulator.
	uint32_t acc=0xffff;

	// Handle complete 16-bit blocks.
	for (size_t i=0;i+1<length;i+=2) {
		uint16_t word;
		memcpy(&word,data+i,2);
		acc+=ntohs(word);
		if (acc>0xffff) {
			acc-=0xffff;
		}
	}

	// Handle any partial block at the end of the data.
	if (length&1) {
		uint16_t word=0;
		memcpy(&word,data+length-1,1);
		acc+=ntohs(word);
		if (acc>0xffff) {
			acc-=0xffff;
		}
	}

	// Return the checksum in network byte order.
	return htons(~acc);
}

uint16_t tcp_checksum(uint32_t source, uint32_t destination, struct my_tcp_header* tcp) {

	size_t taille_datagram = sizeof(struct faux_header)+sizeof(struct my_tcp_header);
	void* datagram = calloc(1, taille_datagram);

    struct faux_header* f = (struct faux_header*) datagram;

    f->source = source;
    f->destination = destination;
    f->protocol = IPPROTO_TCP;
    f->tcp_len = htons(sizeof(struct my_tcp_header));

    struct my_tcp_header* tcp_copy = (struct my_tcp_header*) (datagram + sizeof(struct faux_header));

    memcpy(tcp_copy, tcp, sizeof(struct my_tcp_header));

    //hexDump("datagram ", datagram, taille_datagram);

    return ip_checksum(datagram, taille_datagram);

    free(datagram);

}