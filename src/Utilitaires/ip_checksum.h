#ifndef IP_CHEKSUM_H
#define IP_CHEKSUM_H

#include <stdio.h>
#include <stdint.h>
#include <netinet/tcp.h>

#include "../Utilitaires/myTCP.h"

uint16_t ip_checksum(void* vdata,size_t length);
uint16_t tcp_checksum(uint32_t source, uint32_t destination, struct my_tcp_header* tcp);

#endif