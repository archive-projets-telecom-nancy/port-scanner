#ifndef MYTCP_H
#define MYTCP_H

#include <stdint.h>

struct my_tcp_header {
	uint16_t source:16;
	uint16_t dest:16;
	uint32_t seq:32;
	uint32_t ack_seq:32;
	uint8_t  doff:8;
	uint8_t  flags:8;
	uint16_t window:16;
	uint16_t check:16;
	uint16_t urg_ptr:16;
};

struct faux_header {
	uint32_t source;
	uint32_t destination;
	uint8_t  _;
	uint8_t  protocol;
	uint16_t tcp_len;
};

void print_tcp(struct my_tcp_header*);

#endif