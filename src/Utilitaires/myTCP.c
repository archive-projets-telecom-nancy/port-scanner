#include <stdio.h>

#include "myTCP.h"

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

void print_tcp(struct my_tcp_header* tcp) {
	printf("{\n");
	printf("\tsource  : %d\n",tcp->source);
	printf("\tdest    : %d\n",tcp->dest);
	printf("\tseq     : %d\n",tcp->seq);
	printf("\tack_seq : %d\n",tcp->ack_seq);
	printf("\tdoff    : "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(tcp->doff));
	printf("\tflags   : "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(tcp->flags));
	printf("\twindow  : %d\n",tcp->window);
	printf("\tcheck   : %d\n",tcp->check);
	printf("\turg_ptr : %d\n",tcp->urg_ptr);
	printf("}\n");
}