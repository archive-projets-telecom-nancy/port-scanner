#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/ip.h>

#include "../TCP_Scan/TCP_SYN_Scan.h"
#include "../Utilitaires/ip_checksum.h"
#include "../Utilitaires/myTCP.h"

int syn_ack(char* destination, int dest_port) {
	struct in_addr dest_ip;
	//La socket pour récupérer les réponses
	int sckt = socket(AF_INET, SOCK_RAW , IPPROTO_TCP);

	if (sckt<0)
	{
		printf("Erreur à l'ouverture de la socket.\n");
		exit(0);
	}

    char datagram[4096];    
     
    //Header du paquet TCP
    struct my_tcp_header *tcph = (struct my_tcp_header *) (datagram + sizeof(struct ip));
    
    //Adresse d'émission et de réception
    struct sockaddr_in dest;
    socklen_t len=sizeof(dest);
    char buffer[1500];

    //On met la bonne adresse pour l'envoi du paquet
	if (inet_addr(destination) > 0){
		dest_ip.s_addr = inet_addr(destination);	
	}
	else
	{
		printf("Adresse invalide\n");
		exit(1);
	}


    int port_source = 55555;
    memset(datagram, 0, 4096);

    //On remplit le header du paquet TCP
    tcph->source = htons (port_source);
    tcph->dest = htons (dest_port);
    tcph->seq = htonl(1105024978);
    tcph->ack_seq = 0;
    tcph->doff = 0b01010000; // data offset de 20
    tcph->flags = 0b00010001; // SYN et ACK
    tcph->window = 0;
    tcph->check = 0;
    tcph->urg_ptr = 0;

    //Configuration de la socket
    int on = 1;
    if (setsockopt (sckt, IPPROTO_IP, SOCK_RAW, &on, sizeof (on)) < 0)
    {
        printf("Erreur de setsockopt\n");
        exit(1);
    }

    dest.sin_addr.s_addr = dest_ip.s_addr;
    dest.sin_family = AF_INET;

    //On calcule le checksum
    tcph->check = tcp_checksum(recuperer_adresse_source(), (uint32_t) dest_ip.s_addr,tcph);

    if ( sendto (sckt, datagram , sizeof(datagram) , 0 , (struct sockaddr *) &dest, sizeof (dest)) < 0)
    {
        printf ("Erreur d'envoi du paquet SYN.\n");
        exit(0);
    }

    int sckt_raw = socket(AF_INET, SOCK_RAW , IPPROTO_TCP);

    if (sckt<0)
	{
		printf("Erreur à l'ouverture de la socket RAW.\n");
		exit(0);
	}

    int sizeData = recvfrom (sckt_raw,&buffer, sizeof(buffer),0, (struct sockaddr *)&dest, &len);

    if (sizeData < 0){
    	printf("Erreur de réception du paquet\n");
    	exit(1);
    }
    

    struct iphdr *iph = (struct iphdr*)buffer;
    struct sockaddr_in from,to;
    int iphdrlen = iph->ihl*4;

    tcph = (struct my_tcp_header*)(buffer + iphdrlen);

    memset(&from, 0, sizeof(from));
    from.sin_addr.s_addr = iph->saddr;
 
    memset(&to, 0, sizeof(to));
    to.sin_addr.s_addr = iph->daddr;
     
    if(tcph->flags&0b00000100 && from.sin_addr.s_addr == dest_ip.s_addr )
    {
        printf("Port %d open \n" , ntohs(tcph->source));
        fflush(stdout);
    }


	return 0;
}