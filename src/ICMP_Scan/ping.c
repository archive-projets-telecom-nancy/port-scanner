#include <stdio.h>
#include <stdlib.h>

#include "ping.h"

ping* new_ping() {

	ping* p;
	ping* tmp = (ping*) calloc(1,sizeof(ping));
	if (tmp == NULL) {
		printf("Erreur lors de l'allocation d'un ping");
		free(tmp);
		exit(-1);
	} else {
		p = tmp;
	}

	p->status = PAS_ENVOYE;

	return p;
}

int ping_pas_encore_envoye(void* ping_raw) {
	ping* p = (ping*) ping_raw;
	return p->status == PAS_ENVOYE;
}

int ping_en_cours(void* ping_raw) {
	ping* p = (ping*) ping_raw;
	return !(p->status == RECU || p->status == TIMEOUT);
}

int addr_comp(void* a,void* b) {
	uint32_t* a_ = (uint32_t*) a;
	uint32_t* b_ = (uint32_t*) b;
	return *a_ == *b_;
}

void free_ping(void* ping_raw) {
	ping* p = (ping*) ping_raw;
	free(p);
}