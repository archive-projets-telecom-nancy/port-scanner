#ifndef PING_SWEEP_H
#define PING_SWEEP_H

#include <time.h>

#include "../Utilitaires/vecteur.h"
#include "ping.h"

vecteur* ping_sweep_retry(uint32_t debut_reverse, uint32_t fin_reverse, double limite_timeout_s, double* ping_interval_s, int essais);
vecteur* ping_sweep(uint32_t debut_reverse, uint32_t fin_reverse, double limite_timeout_s, double* ping_interval_s);
int creer_socket_raw();
int envoyer_ping(int sock_fd, ping* p);
int recevoir_pong(int sock_fd,vecteur* pings);
int pong_recu(vecteur* pings,uint32_t adresse,uint16_t identifiant);
int verifier_timeouts(vecteur* pings, double limite_timeout_s);
int out_of_time(ping* p, double limite_timeout_s);
int scan_termine(vecteur* pings);
int its_ping_time(clock_t last_ping, clock_t ping_interval_s);
int trouver_prochain_ping(vecteur* pings);

#endif