#ifndef PING_H
#define PING_H

#include <time.h>
#include <stdint.h>

enum ping_status {
	PAS_ENVOYE,
	ENVOYE,
	RECU,
	TIMEOUT
};

typedef struct ping ping;

struct ping {
	uint32_t adresse;
	enum ping_status status;
	uint16_t identifiant;
	clock_t temps_envoi;
};

ping* new_ping();
int ping_en_cours(void*);
int ping_pas_encore_envoye(void*);
int addr_comp(void*,void*);
void free_ping(void*);

#endif