#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "ping_sweep.h"
#include "../Utilitaires/vecteur.h"
#include "../Utilitaires/ip_checksum.h"
#include "ping.h"

vecteur* ping_sweep_retry(uint32_t debut_reverse, uint32_t fin_reverse, double limite_timeout_s, double* ping_interval_s, int essais) {
	
	vecteur* res = new_vecteur(sizeof(uint32_t));
	for (int i = 0; i < essais; ++i) {
		printf("\rVague %d/%d ...",i+1,essais);
		fflush(stdout);
		vecteur* res_vague = ping_sweep(debut_reverse,fin_reverse,limite_timeout_s,ping_interval_s);
		vect_merge_unique(res,res_vague,addr_comp);
		vect_free(res_vague,free_ping);
	}
	printf("\n");
	return res;
}

vecteur* ping_sweep(uint32_t debut_reverse, uint32_t fin_reverse, double limite_timeout_s, double* ping_interval_s) {

	int sock_fd = creer_socket_raw();
	vecteur* pings = new_vecteur(sizeof(ping));
	vecteur* adresses = new_vecteur(sizeof(uint32_t));

	for (uint32_t i = debut_reverse; i <= fin_reverse; i++) {
		uint32_t adresse = htonl(i);
		vect_copy_and_append(adresses, &adresse);
	}
	long int intervalle_envoi = 5;
	clock_t last_ping = clock();
	int indice_adresse = 0;
	int cibles_restantes = adresses->taille;

	while (cibles_restantes > 0) {

		//envoi des pings
		if (indice_adresse < adresses->taille && its_ping_time(last_ping,intervalle_envoi)) {
			ping* p = new_ping();
			memcpy(&p->adresse, adresses->contenu[indice_adresse], sizeof(u_int32_t));
			vect_append(pings, p);
			int resultat = envoyer_ping(sock_fd, p);
			if (resultat != -1) {
				p->status = ENVOYE;
				p->temps_envoi = clock();
				last_ping = clock();
				indice_adresse++;
			} else {
				free_ping(p);
				pings->taille -= 1;
				printf("Intervalle d'envoi : %ld -> ",intervalle_envoi);
				intervalle_envoi *= 2;
				printf("%ld\n",intervalle_envoi);
				fflush(stdout);
			}
		}

		//vérification des réponses
		cibles_restantes -= recevoir_pong(sock_fd,pings);

		//mise à jour des timeout
		cibles_restantes -= verifier_timeouts(pings,limite_timeout_s);

	}

	vect_free(adresses, NULL);

	//formation du vecteur d'adresses ayant répondu
	vecteur* rep = new_vecteur(sizeof(uint32_t));
	for (int i = 0; i < pings->taille; i++) {
		ping* p = (ping*) pings->contenu[i];
		if (p->status == RECU) {
			vect_copy_and_append(rep, &(p->adresse));
		}
	}
	vect_free(pings, free_ping);
	
	return rep;

}

int creer_socket_raw() {

	int socket_raw;
	if((socket_raw=socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0) {
		perror("Erreur lors de la création de la socket ");
		exit(-1);
	}
	if(fcntl(socket_raw, F_SETFL, O_NONBLOCK) < 0) {
		perror("Erreur lors de l'ajout de l'option non bloquante ");
		exit(-1);
	}
	return socket_raw;

}

int envoyer_ping(int sock_fd, ping* p) {

	struct in_addr* dest_addr = (struct in_addr*) &(p->adresse);
	
	p->identifiant = (uint16_t) htons(rand());

	//char buff[16];

	const size_t req_size=8;
	struct icmphdr req;
	req.type=ICMP_ECHO;
	req.code=0;
	req.checksum=0;
	req.un.echo.id=p->identifiant;
	req.un.echo.sequence=htons(1);
	req.checksum=ip_checksum(&req,req_size);

	struct sockaddr_in servaddr;
	memset(&servaddr, 0, sizeof(struct sockaddr_in));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = dest_addr->s_addr;

	//printf("Envoi à %s\n",inet_ntop(AF_INET,(struct in_addr*) dest_addr,buff,15));
	if (sendto(sock_fd, &req, req_size, 0, (struct sockaddr *)&servaddr, sizeof(struct sockaddr_in)) < 0) {
		perror("Erreur lors de l'envoi d'un paquet echo");
		return -1;
	}

	return 0;

}

int recevoir_pong(int sock_fd,vecteur* pings) {

	void* buffer = (void*) malloc(sizeof(struct iphdr)+sizeof(struct icmphdr));

	struct sockaddr_storage adresse_cible;
	socklen_t taille_adresse_cible = sizeof(struct sockaddr_storage);

	//char buff[16];

	int res = 0;

	while (
		recvfrom(
			sock_fd,
			buffer,
			sizeof(struct iphdr)+sizeof(struct icmphdr),
			0,
			(struct sockaddr*) &adresse_cible,
			&taille_adresse_cible
		)
		> 0
	) {
		struct iphdr* ip_rep = (struct iphdr*) buffer;
		struct icmphdr* icmp_rep = (struct icmphdr*) (buffer+sizeof(struct iphdr));
		switch (icmp_rep->type) {

			case ICMP_ECHO:
				//printf("ECHO de %s\n",inet_ntop(AF_INET,(struct in_addr*) &(ip_rep->saddr),buff,15));
				break;
			case ICMP_ECHOREPLY:
				//printf("REPLY de %s\n",inet_ntop(AF_INET,(struct in_addr*) &(ip_rep->saddr),buff,15));
				res += pong_recu(pings,ip_rep->saddr,icmp_rep->un.echo.id);
				break;
			default:
				break;
				//printf("??? de %s\n",inet_ntop(AF_INET,(struct in_addr*) &(ip_rep->saddr),buff,15));
		}
	}

	free(buffer);

	return res;
}

int pong_recu(vecteur* pings,uint32_t adresse,uint16_t identifiant) {
	int res =0;
	for (int i = 0; i < pings->taille; i++)	{
		ping* p = (ping*) pings->contenu[i];
		if (p->adresse == adresse && p->identifiant == identifiant)	{
			p->status = RECU;
			res++;
		}
	}
	return res;
}

int verifier_timeouts(vecteur* pings,double limite_timeout_s) {

	int res = 0;
	for (int i = 0; i < pings->taille; i++)	{
		ping* p = (ping*) pings->contenu[i];
		if (out_of_time(p,limite_timeout_s)) {
			res++;
			p->status = TIMEOUT;
		}
	}
	return res;
}

int out_of_time(ping* p, double limite_timeout_s) {
	if (p->status == ENVOYE) {
		clock_t delta_raw = clock()- p->temps_envoi;
		double delta_s = ((double) (delta_raw / CLOCKS_PER_SEC));
		return delta_s > limite_timeout_s;
	} else {
		return 0;
	}
}


int scan_termine(vecteur* pings) {
	return (vect_first_match(pings,ping_en_cours) == -1);
}

int its_ping_time(clock_t last_ping, clock_t ping_interval_s) {
	clock_t delta_raw = clock() - last_ping;
	return delta_raw > ping_interval_s;
}

int trouver_prochain_ping(vecteur* pings) {
	return vect_first_match(pings, ping_pas_encore_envoye);
}