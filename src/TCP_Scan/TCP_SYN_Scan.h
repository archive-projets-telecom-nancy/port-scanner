#ifndef TCP_SYN_SCAN_H
#define TCP_SYN_SCAN_H

#include <stdint.h>
#include <netinet/ip.h>

#include "SYN_ping.h"
#include "../Utilitaires/myTCP.h"

#include "../Utilitaires/vecteur.h"

void TCP_SYN_Scan(vecteur* adresses, double limite_timeout_s);
vecteur* TCP_SYN_Scan_unique(uint32_t source, uint32_t destination, double limite_timeout_s);
int envoyer_syn_ping(int sock_fd, uint32_t source, syn_ping* sp);
int creer_socket();
int peut_envoyer(clock_t dernier_envoi, int intervalle_envoi);
uint32_t recuperer_adresse_source();
int recevoir_syn_pong(int sock_fd,vecteur* syn_ping);
int syn_pong_recu(vecteur* syn_pings,struct iphdr* ip, struct my_tcp_header* tcp);
int syn_verifier_timeouts(vecteur* syn_pings,double limite_timeout_s);
int syn_out_of_time(syn_ping* p, double limite_timeout_s);

#endif