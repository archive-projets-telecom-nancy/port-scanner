#ifndef SYN_PING_H
#define SYN_PING_H

#include <time.h>
#include <stdint.h>

enum syn_ping_status {
	SYN_PAS_ENVOYE,
	SYN_ENVOYE,
	RECU_OUVERT,
	RECU_FERME,
	SYN_TIMEOUT
};

typedef struct syn_ping syn_ping;

struct syn_ping {
	uint32_t adresse_cible;
	uint16_t port_origine;
	uint16_t port_cible;
	enum syn_ping_status status;
	uint32_t seq;
	clock_t temps_envoi;
};

syn_ping* new_syn_ping();
void free_syn_ping(void* sp);

#endif