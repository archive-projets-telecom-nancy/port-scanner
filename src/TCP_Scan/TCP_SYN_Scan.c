#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/ip.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "TCP_SYN_Scan.h"
#include "SYN_ping.h"
#include "../Utilitaires/myTCP.h"
#include "../Utilitaires/vecteur.h"
#include "../Utilitaires/ip_checksum.h"
#include "../Utilitaires/hex_dump.h"

extern int nombre_de_ports;
extern uint16_t ports[];

void TCP_SYN_Scan(vecteur* adresses, double limite_timeout_s) {

	char buff[16];
	uint32_t source = recuperer_adresse_source();
	
	for (int i = 0; i < adresses->taille; i++) {
		uint32_t* adresse = adresses->contenu[i];
		printf("Scan TCP SYN de %s\n",inet_ntop(AF_INET,(struct in_addr*) adresse,buff,16));
		vecteur* resultats = TCP_SYN_Scan_unique(source, *adresse, limite_timeout_s);
		for (int i = 0; i < resultats->taille; i++) {
			uint16_t* port = (uint16_t*) resultats->contenu[i];
			printf("Port ouvert : %d\n",ntohs(*port));
		}
		vect_free(resultats,free_syn_ping);
	}
}

vecteur* TCP_SYN_Scan_unique(uint32_t source, uint32_t destination, double limite_timeout_s) {

	clock_t intervalle_envoi = 5;
	int sock_fd = creer_socket();
	vecteur* syn_pings = new_vecteur(sizeof(syn_ping));

	clock_t dernier_envoi = clock();
	int indice_port = 0;
	int ports_restant = 1000;

	while (ports_restant > 0) {
		if(indice_port < nombre_de_ports && peut_envoyer(dernier_envoi, intervalle_envoi)) {
			syn_ping* sp = new_syn_ping();
			sp->adresse_cible = destination;
			sp->port_origine = htons(rand());
			sp->port_cible = htons(ports[indice_port]);
			sp->seq = htonl(rand());
			vect_append(syn_pings, sp);
			if(envoyer_syn_ping(sock_fd,source,sp) >= 0) {
				sp->status = SYN_ENVOYE;
				sp->temps_envoi = clock();
				dernier_envoi = clock();
				indice_port++;
				printf("\r%d/1000",indice_port);
			} else {
				free_syn_ping(sp);
				syn_pings->taille -= 1;
				printf("Intervalle d'envoi : %ld -> ",intervalle_envoi);
				intervalle_envoi *= 2;
				printf("%ld\n",intervalle_envoi);
				fflush(stdout);
			}
		}
		//vérification des réponses
		ports_restant -= recevoir_syn_pong(sock_fd,syn_pings);

		//mise à jour des timeout
		ports_restant -= syn_verifier_timeouts(syn_pings,limite_timeout_s);
	}
	printf("\n");

	vecteur* ports_ouverts = new_vecteur(sizeof(uint16_t));

	for (int i = 0; i < syn_pings->taille; i++)	{
		syn_ping* sp = (syn_ping*) syn_pings->contenu[i];
		if (sp->status == RECU_OUVERT) {
			vect_copy_and_append(ports_ouverts, &(sp->port_cible));
		}
	}

	vect_free(syn_pings, free_syn_ping);

	return ports_ouverts;
}

int envoyer_syn_ping(int sock_fd, uint32_t source, syn_ping* sp) {

	// formation du paquet
	struct my_tcp_header message;

	size_t taille_tcp = sizeof(struct my_tcp_header);
	message.source = sp->port_origine;
	message.dest = sp->port_cible;
	message.seq = sp->seq;
	message.ack_seq = 0;
	message.doff = 0b01010000;
	message.flags = 0b00000010;
	message.window = 0;
	message.check = 0;
	message.urg_ptr = 0;
	message.check = tcp_checksum(source, sp->adresse_cible, &message);

	// réglage de l'adresse
	struct in_addr* dest_addr = (struct in_addr*) &(sp->adresse_cible);

	struct sockaddr_in servaddr;
	memset(&servaddr, 0, sizeof(struct sockaddr_in));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = dest_addr->s_addr;

	return sendto (
		sock_fd,
		&message,
		taille_tcp,
		0,
		(struct sockaddr *)&servaddr,
		sizeof(struct sockaddr_in)
	);

}


int creer_socket() {

	int socket_raw;
	if((socket_raw=socket(AF_INET, SOCK_RAW, IPPROTO_TCP)) < 0) {
		perror("Erreur lors de la création de la socket ");
		exit(-1);
	}
	if(fcntl(socket_raw, F_SETFL, O_NONBLOCK) < 0) {
		perror("Erreur lors de l'ajout de l'option non bloquante ");
		exit(-1);
	}
	return socket_raw;

}

int peut_envoyer(clock_t dernier_envoi, int intervalle_envoi) {
	int delta = (int) clock() - dernier_envoi;
	return delta > intervalle_envoi;
}

uint32_t recuperer_adresse_source() {

    int sock = socket(AF_INET, SOCK_DGRAM, 0);

    char* kGoogleDnsIp = "8.8.8.8";
    int dns_port = 53;
 
    struct sockaddr_in serv;
 
    memset(&serv, 0, sizeof(serv));
    serv.sin_family = AF_INET;
    serv.sin_addr.s_addr = inet_addr(kGoogleDnsIp);
    serv.sin_port = htons(dns_port);
 
    if (connect(sock, (const struct sockaddr*) &serv, sizeof(serv)) < 0) {
    	perror("Impossible de se connecter au DNS de Google pour récuperer mon IP");
    	exit(-1);
    }
 
    struct sockaddr_in name;
    socklen_t namelen = sizeof(name);
    if (getsockname(sock, (struct sockaddr*) &name, &namelen) < 0) {
    	perror("Impossible de récuperer l'adresse depuis la socket");
    	exit(-1);
    }

    if (name.sin_family != AF_INET) {
    	printf("Impossible de récuperer une IPv4 pour ma propre adresse !\n");
    	exit(-1);
    }

    close(sock);

    uint32_t* adresse = (uint32_t*) &(name.sin_addr);

    /*
    char buff[16];
    printf("Adresse trouvée : %s\n",inet_ntop(AF_INET,(struct in_addr*) adresse,buff,15));
 	*/

    return *adresse;   
}

int recevoir_syn_pong(int sock_fd, vecteur* syn_pings) {

	size_t taille_buffer = sizeof(struct iphdr)+sizeof(struct my_tcp_header);
	void* buffer = (void*) calloc(1,taille_buffer);

	struct sockaddr_storage adresse_cible;
	socklen_t taille_adresse_cible = sizeof(struct sockaddr_storage);

	//char buff[16];

	int res = 0;

	while (
		recvfrom(
			sock_fd,
			buffer,
			taille_buffer,
			0,
			(struct sockaddr*) &adresse_cible,
			&taille_adresse_cible
		)
		> 0
	) {
		struct iphdr* ip_rep = (struct iphdr*) buffer;
		struct my_tcp_header* tcp_rep = (struct my_tcp_header*) (buffer+sizeof(struct iphdr));
		res += syn_pong_recu(syn_pings,ip_rep,tcp_rep);
	}

	free(buffer);
	return res;
}

int syn_pong_recu(vecteur* syn_pings,struct iphdr* ip, struct my_tcp_header* tcp) {
	int res = 0;
	for (int i = 0; i < syn_pings->taille; i++)	{
		syn_ping* sp = (syn_ping*) syn_pings->contenu[i];

		//Si c'est la bonne IP et que c'est envoyé au bon port
		if ((sp->adresse_cible == ip->saddr)
			&& (sp->port_origine == tcp->dest)
			&& (sp->port_cible == tcp->source)) {

			/*
			int RST = tcp->flags & 0b00000100;
			int SYN = tcp->flags & 0b00000010;
			int ACK = tcp->flags & 0b00010000;
			*/

			switch(tcp->flags) {
				case 4:
					sp->status = RECU_FERME;
					res++;
					break;
				case 18:
					//hexDump("SYN/ACK recu ", tcp, sizeof(struct my_tcp_header));
					sp->status = RECU_OUVERT;
					res++;
					break;
				default:
					break;
			}
		}
	}
	return res;
}

int syn_verifier_timeouts(vecteur* syn_pings,double limite_timeout_s) {

	int res = 0;
	for (int i = 0; i < syn_pings->taille; i++)	{
		syn_ping* sp = (syn_ping*) syn_pings->contenu[i];
		if (syn_out_of_time(sp,limite_timeout_s)) {
			res++;
			sp->status = SYN_TIMEOUT;
		}
	}
	return res;
}

int syn_out_of_time(syn_ping* p, double limite_timeout_s) {
	if (p->status == SYN_ENVOYE) {
		clock_t delta_raw = clock()- p->temps_envoi;
		double delta_s = ((double) (delta_raw / CLOCKS_PER_SEC));
		return delta_s > limite_timeout_s;
	} else {
		return 0;
	}
}