#include <stdio.h>
#include <stdlib.h>

#include "SYN_ping.h"

syn_ping* new_syn_ping() {

	syn_ping* sp;
	syn_ping* tmp = (syn_ping*) calloc(1,sizeof(syn_ping));
	if (tmp == NULL) {
		printf("Erreur lors de l'allocation d'un ping");
		free(tmp);
		exit(-1);
	} else {
		sp = tmp;
	}

	sp->status = SYN_PAS_ENVOYE;

	return sp;

}

void free_syn_ping(void* sp) {
	syn_ping* sp_ = (syn_ping*) sp;
	free(sp_);
}