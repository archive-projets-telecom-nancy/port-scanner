#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>

#include "ICMP_Scan/ping_sweep.h"
#include "TCP_Scan/TCP_SYN_Scan.h"
#include "TCP_Idle_Scan/TCP_Idle_Scan.h"
#include "UDP_Scan/UDP_Scan.h"


int main(int argc, char** argv) {

	int TCP_SYN = 0;
	int TCP_Idle = 0;
	int UDP_Scan = 0;
	uint32_t hote_zombie;
	uint16_t port_zombie;
	uint16_t port_udp;

	char* start_ip = NULL;
	char* end_ip = NULL;
	char* zombie_host = NULL;
	char* zombie_port = NULL;
	char* udp_host = NULL;
	char* udp_port = NULL;

	for (int i = 0; i < argc; i++) {
		if (strcmp(argv[i], "--start") == 0) {
			start_ip = argv[++i];
		} else if (strcmp(argv[i], "--end") == 0) {
			end_ip = argv[++i];
		} else if (strcmp(argv[i], "--SYN") == 0) {
			TCP_SYN = 1;
		} else if (strcmp(argv[i], "--IDLE") == 0) {
			TCP_Idle = 1;
		} else if (strcmp(argv[i], "--z_host") == 0) {
			zombie_host = argv[++i];
		} else if (strcmp(argv[i], "--z_port") == 0) {
			zombie_port = argv[++i];
		} else if (strcmp(argv[i], "--UDP") == 0) {
			UDP_Scan = 1;
			udp_host = argv[++i];
			udp_port = argv[++i];
		}
	}

	if (start_ip == NULL || end_ip == NULL) {
		printf("Usage : %s --start [ip] --end [ip] [--SYN|--Idle --z_host [ip] --zport [port]|--UDP [ip] [port]]\n",argv[0]);
		return -1;
	}

	if (TCP_Idle && (zombie_host == NULL || zombie_port == NULL)) {
		printf("Usage : %s --start [ip] --end [ip] [--SYN|--Idle --z_host [ip] --zport [port]|--UDP [ip] [port]]\n",argv[0]);
		return -1;
	}

	if (UDP_Scan && (udp_host == NULL || udp_port == NULL)) {
		printf("Usage : %s --start [ip] --end [ip] [--SYN|--Idle --z_host [ip] --zport [port]|--UDP [ip] [port]]\n",argv[0]);
		return -1;
	}


	struct in_addr start_addr;
	struct in_addr end_addr;
	struct in_addr zombie_addr;

	if(inet_pton(AF_INET, start_ip, &start_addr) == 0) {
		printf("Impossible de parser l'adresse de début\n");
		exit(-1);
	}

	if(inet_pton(AF_INET, end_ip, &end_addr) == 0) {
		printf("Impossible de parser l'adresse de fin\n");
		exit(-1);
	}

	if (TCP_Idle) {
		if(inet_pton(AF_INET, zombie_host, &zombie_addr) == 0) {
			printf("Impossible de parser l'adresse du zombie\n");
			exit(-1);
		}
		hote_zombie = htonl(zombie_addr.s_addr);
		port_zombie = (short) atoi(zombie_port);
	}

	if (UDP_Scan) {
		port_udp = (short) atoi(udp_port);
	}

	uint32_t start = htonl(start_addr.s_addr);
	uint32_t end = htonl(end_addr.s_addr);

	if (start > end) {
		printf("Le début est avant la fin sur la plage spécifiée !\n");
		exit(-1);
	}

	if (end-start > 65535) {
		printf("Plage d'adresses trop grande !\n");
		exit(-1);
	}

	// déterminer les adresses IP des machines actives
	printf("ICMP Sweep Scan\n");
	vecteur* resultats = ping_sweep_retry(start,end,(double) 1.0,NULL,5);

	char buff[16];

	for (int i = 0; i < resultats->taille; i++)	{
		uint32_t* res = (uint32_t*) resultats->contenu[i];
		printf("Recu : %s\n",inet_ntop(AF_INET,(struct in_addr*) res,buff,16));
	}
	
	// déterminer et afficher les ports ouverts en TCP
	if (TCP_SYN) {
		TCP_SYN_Scan(resultats, (double) 2.0);
	}

	if (TCP_Idle) {
		TCP_Idle_Scan(resultats, hote_zombie, port_zombie);
	}

	if (UDP_Scan) {
		udp_scan(udp_host, port_udp);
	}
	// déterminer et afficher les ports ouverts en UDP

	// déterminer la version du service + le système d’exploitation

	return 0;
}
