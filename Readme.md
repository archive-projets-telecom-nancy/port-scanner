Projet de RSA MyNmap
===

Syméon CARLE et Pierre DENAUW
---

# Compilation du projet

pour compiler le projet il suffit de lancer une commande make dans le dossier src/

La compilation a été testée avec gcc (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 uniquement

Nous ne garantissons en rien son fonctionnement sur mac ou autre OS unix, et encore moins sous windows

# Lancement des commandes

notre version de mynmap s'utilise de la manière suivante :

`sudo mynmap --start x.x.x.x --end y.y.y.y`

Il effectue alors un scan ICMP de l'adresse x.x.x.x jusqu'a l'adresse y.y.y.y

pour effectuer un scan TCP SYN il faut rajouter l'option `--SYN`

pour un scan TCP Idle il faut rajouter `--IDLE` et également `--z_host z.z.z.z --z_port 00000` pour spécifier une machine zombie à utiliser ainsi qu'un port car mynmap ne gère pas dans sa version actuelle la recherche de machine zombie convenable

pour un scan UDP il faut ajouter `--UDP` suivi de l'ip et du port