#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "../../src/SYNACK/synack_scan.h"

int main(int argc, char const *argv[])
{
	if (argc != 2) {
		printf("Usage : %s [IPv4]\n",argv[0]);
		return -1;
	}

	char* ip_string= argv[1];

	struct in_addr ip_addr;

	if(inet_pton(AF_INET, ip_string, &ip_addr) == 0) {
		printf("Impossible de parser l'adresse !\n");
		return -1;
	}

	uint32_t source = recuperer_adresse_source();
	
	for (int i = 0; i < 10; i++) {
		syn_ack(ip_string,i);
	}
	return 0;
}