#ifndef TEST_UN_SEUL_H
#define TEST_UN_SEUL_H

#include <unistd.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>

void montrer_reponses(int sock_fd);
void hexDump (char *desc, void *addr, int len);
void print_iphdr(struct iphdr* ip);
void print_icmphdr(struct icmphdr* icmp);

#endif