#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h> //inet_ntop inet_pton
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>

#include "../../src/1._IP_scan.h"
#include "test_un_seul.h"

int main() {

	printf("Création de la socket\n");
	int sock_fd = creer_socket_raw();

	printf("Envoi de l'echo\n");

	char buff[16];
	struct in_addr adresse_source;
	for (int i = 1; i < 255; i++) {
		sprintf(buff, "192.168.1.%d", i);
		inet_pton(AF_INET, buff, &adresse_source);
		envoyer_echo(sock_fd, &(adresse_source.s_addr), (uint16_t) 75, 0);
	}
	/*
	inet_pton(AF_INET, "192.168.1.1", &adresse_source);
	envoyer_echo(sock_fd, &(adresse_source.s_addr), (uint16_t) 75, 0);
	envoyer_echo(sock_fd, &(adresse_source.s_addr), (uint16_t) 76, 0);
	envoyer_echo(sock_fd, &(adresse_source.s_addr), (uint16_t) 77, 0);
	*/
	printf("Réponses :\n\n");
	clock_t start = clock();
	while ( ((double) ((clock()-start)/ CLOCKS_PER_SEC)) < 5.0) {
		montrer_reponses(sock_fd);	
	}

	return 0;
}

void montrer_reponses(int sock_fd) {

	void* buffer = (void*) malloc(sizeof(struct iphdr)+sizeof(struct icmphdr));

	struct sockaddr_storage adresse_cible;
	socklen_t taille_adresse_cible = sizeof(struct sockaddr_storage);

	char buff[16];

	while (
		recvfrom(
			sock_fd,
			buffer,
			sizeof(struct iphdr)+sizeof(struct icmphdr),
			0,
			(struct sockaddr*) &adresse_cible,
			&taille_adresse_cible
		)
		> 0
	) {

		//hexDump("Hex dump ",buffer,sizeof(struct iphdr)+sizeof(struct icmphdr));
		struct iphdr* ip_rep = (struct iphdr*) buffer;
		struct icmphdr* icmp_rep = (struct icmphdr*) (buffer+sizeof(struct iphdr));
		switch (icmp_rep->type) {

			case ICMP_ECHO:
				printf("ECHO de %s\n",inet_ntop(AF_INET,(struct in_addr*) &(ip_rep->saddr),buff,15));
				break;
			case ICMP_ECHOREPLY:
				printf("REPLY de %s\n",inet_ntop(AF_INET,(struct in_addr*) &(ip_rep->saddr),buff,15));
				break;
			default:
				printf("??? de %s\n",inet_ntop(AF_INET,(struct in_addr*) &(ip_rep->saddr),buff,15));
		}
		//print_iphdr(ip_rep);
		//print_icmphdr(icmp_rep);
		fflush(stdout);
	}
	
	/*
	if (reponse > 0) {
		hexDump("Hex dump ",buffer,sizeof(struct iphdr)+sizeof(struct icmphdr));
		struct iphdr* ip_rep = (struct iphdr*) buffer;
		struct icmphdr* icmp_rep = (struct icmphdr*) (buffer+sizeof(struct iphdr));
		print_iphdr(ip_rep);
		print_icmphdr(icmp_rep);
		hexDump("icmp",&(icmp_rep),sizeof(struct icmphdr));
	}*/
}

void print_iphdr(struct iphdr* ip) {

	char buff[16];

	printf("IP header :\n");
	printf("{\n");
	printf("    destination : %s\n",inet_ntop(AF_INET,(struct in_addr*) &(ip->daddr),buff,15));
	printf("    source      : %s\n",inet_ntop(AF_INET,(struct in_addr*) &(ip->saddr),buff,15));
	printf("}\n");
}

void print_icmphdr(struct icmphdr* icmp) {
	printf("ICMP header :\n");
	printf("{\n");
	printf("    type       : %d\n",icmp->type);
	printf("    code       : %d\n",icmp->code);
	printf("    un.echo.id : %d\n",icmp->un.echo.id);
	printf("}\n");
}

void hexDump (char *desc, void *addr, int len) {
    int i;
    unsigned char buff[17];
    unsigned char *pc = (unsigned char*)addr;

    // Output description if given.
    if (desc != NULL)
        printf ("%s:\n", desc);

    if (len == 0) {
        printf("  ZERO LENGTH\n");
        return;
    }
    if (len < 0) {
        printf("  NEGATIVE LENGTH: %i\n",len);
        return;
    }

    // Process every byte in the data.
    for (i = 0; i < len; i++) {
        // Multiple of 16 means new line (with line offset).

        if ((i % 16) == 0) {
            // Just don't print ASCII for the zeroth line.
            if (i != 0)
                printf ("  %s\n", buff);

            // Output the offset.
            printf ("  %04x ", i);
        }

        // Now the hex code for the specific character.
        printf (" %02x", pc[i]);

        // And store a printable ASCII character for later.
        if ((pc[i] < 0x20) || (pc[i] > 0x7e))
            buff[i % 16] = '.';
        else
            buff[i % 16] = pc[i];
        buff[(i % 16) + 1] = '\0';
    }

    // Pad out last line if not exactly 16 characters.
    while ((i % 16) != 0) {
        printf ("   ");
        i++;
    }

    // And print the final ASCII bit.
    printf ("  %s\n", buff);
}