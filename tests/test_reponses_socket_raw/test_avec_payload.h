#ifndef TEST_AVEC_PAYLOAD_H
#define TEST_AVEC_PAYLOAD_H

#include <unistd.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>

void montrer_reponses(int sock_fd);
void hexDump (char *desc, void *addr, int len);
void print_iphdr(struct iphdr* ip);
void print_icmphdr(struct icmphdr* icmp);
void envoyer_echo_avec_playload(int sock_fd, char* destination, uint16_t identidiant);

#endif