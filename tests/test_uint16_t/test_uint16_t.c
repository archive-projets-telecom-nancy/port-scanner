#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

struct uint_test_struct {
	uint8_t _8;
	uint16_t _16;
	uint32_t _32;
};

struct limite_chiffre {
	int _8:8;
	int _16:16;
	int _32:32;
};

void print_uint(struct uint_test_struct* u) {
	printf("{\n");
	printf("    _8  : %d\n",u->_8);
	printf("    _16 : %d\n",u->_16);
	printf("    _32 : %d\n",u->_32);
	printf("}\n\n");
}

int main() {

	struct uint_test_struct* uint_test = malloc(sizeof(struct uint_test_struct));
	struct limite_chiffre* chiffre_test = malloc(sizeof(struct limite_chiffre));

	uint_test->_8 = 8;
	uint_test->_16 = 16;
	uint_test->_32 = 32;

	printf("avant :\n");
	print_uint(uint_test);

	uint_test->_8 = 254;
	printf("uint_test->_8 = 254;\n");
	print_uint(uint_test);

	uint_test->_8 = 255;
	printf("uint_test->_8 = 255;\n");
	print_uint(uint_test);

	uint_test->_8 = 256;
	printf("uint_test->_8 = 256;\n");
	print_uint(uint_test);

	uint_test->_8 = 257;
	printf("uint_test->_8 = 257;\n");
	print_uint(uint_test);


}