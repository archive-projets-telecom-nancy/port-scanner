#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "../../src/TCP_Scan/TCP_SYN_Scan.h"


int main(int argc, char** argv) {

	if (argc != 2) {
		printf("Usage : %s [IPv4]\n",argv[0]);
		return -1;
	}

	char* ip_string= argv[1];

	struct in_addr ip_addr;

	if(inet_pton(AF_INET, ip_string, &ip_addr) == 0) {
		printf("Impossible de parser l'adresse !\n");
		return -1;
	}

	uint32_t source = recuperer_adresse_source();
	
	int sock_fd = creer_socket();
	
	for (int i = 0; i < 10; i++) {
		envoyer_SYN(sock_fd, source, ip_addr.s_addr, i);
	}

}