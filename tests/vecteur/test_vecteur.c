#include <stdio.h>
#include <string.h>

#include "../../src/vecteur.h"

void int_printer(void* entier) {
	int* entier_cast = (int*) entier;
	printf("%d",*entier_cast);
}

int main() {

	vecteur* v = new_vecteur(sizeof(int));

	for (int i = 0; i < 10; ++i) {
		vect_copy_and_append(v, &i);
	}

	vect_print(v, int_printer);

	return 0;
}