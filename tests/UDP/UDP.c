#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "../../src/UDP_Scan/UDP_Scan.h"

int main(int argc, char const *argv[])
{
	if (argc != 3) {
		printf("Usage : %s [IPv4] [port]\n",argv[0]);
		return -1;
	}

	char* ip_string= argv[1];
	int port = argv[2];

	struct in_addr ip_addr;

	if(inet_pton(AF_INET, ip_string, &ip_addr) == 0) {
		printf("Impossible de parser l'adresse !\n");
		return -1;
	}
	printf("Adresse parsée\n");
	
	udp_scan(ip_string, port);

	return 0;
}