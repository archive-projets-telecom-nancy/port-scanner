#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>

int main (int argc, char** argv) {

	if (argc != 3) {
		printf("Usage : %s [start ip] [end ip]\nIPv4 only !\n",argv[0]);
		exit(-1);
	}

	char* start_ip = argv[1];
	char* end_ip = argv[2];

	struct in_addr start_addr;
	struct in_addr end_addr;

	if(inet_pton(AF_INET, start_ip, &start_addr) == 0) {
		printf("Impossible de parser l'adresse de début\n");
		exit(-1);
	}

	if(inet_pton(AF_INET, end_ip, &end_addr) == 0) {
		printf("Impossible de parser l'adresse de fin\n");
		exit(-1);
	}

	u_int32_t reverse_start = htonl(start_addr.s_addr);
	u_int32_t reverse_end = htonl(end_addr.s_addr);

	printf("start : %15s\t-> %10u\t~> %10u\n",start_ip,start_addr.s_addr, reverse_start);
	printf("end   : %15s\t-> %10u\t~> %10u\n",end_ip,end_addr.s_addr, reverse_end);


	return 0;
}